﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ClusterItem : MonoBehaviour {

    public GameObject floorForMask;
    public Texture2D mask;

    public int[] Projects;

    GameObject quad;
    GameObject textMesh;
    Camera m_Camera;

    // Use this for initialization
    void Start () {
        m_Camera = Camera.main;
        quad = transform.GetChild(0).gameObject;
        textMesh = quad.transform.GetChild(0).gameObject;
        textMesh.GetComponent<TextMeshPro>().text = gameObject.name;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LateUpdate()
    {
        quad.transform.LookAt(quad.transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
    }

    private void OnMouseUpAsButton()
    {
        Debug.Log(gameObject.name);
    }

    public void SetMask()
    {
        SetProjectFilter();
        //floorForMask.GetComponent<MeshRenderer>().materials[0].SetTexture("_Detail", mask);
    }
    void SetProjectFilter()
    {
        ProjectsScript.instance.ProjectFilterByIndexArray(Projects);
    }
}
