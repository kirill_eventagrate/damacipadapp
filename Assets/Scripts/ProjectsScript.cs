﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectsScript : MonoBehaviour
{

    static public ProjectsScript instance = null;

    public GameObject MainPanel;
    public GameObject Parent;
    public GameObject RowPrefab;
    public Button ShowPanelBtn;
    public Button ShowFavoritesBtn;
    public InputField Search;

    List<Favorites> projectsList = new List<Favorites>();
    GameObject[] projects;
    Vector2 startPos;
    bool isPanelShow = false;

    [HideInInspector]
    public bool wasShowing = false;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Use this for initialization
    void Start () {
        
        startPos = MainPanel.GetComponent<RectTransform>().anchoredPosition;
        ShowPanelBtn.onClick.AddListener(() => ShowProjectsList());
        ShowFavoritesBtn.onClick.AddListener(() => ShowFavorites());
        Search.onValueChanged.AddListener(delegate { SetSearchResult(Search); });
        
        SetProjects();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetProjects()
    {
        projects = GameObject.FindGameObjectsWithTag("Project");
        foreach (GameObject proj in projects)
        {
            string _name = proj.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Point>().Name;
            string _path = proj.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Point>().Path;
            int _type = proj.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Point>().Type;
            int _template = proj.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Point>().Template;
            projectsList.Add(new Favorites(_name, _path, _type, _template));
        }
        SetList(projectsList.ToArray());
    }

    public void SetProjects(List<Favorites> array)
    {
        projectsList = array;
        SetList(projectsList.ToArray(), true);
    }

    public void SetList(Favorites[] array)
    {
        for (int i = 0; i < Parent.transform.childCount; i++)
        {
            Destroy(Parent.transform.GetChild(i).gameObject);
        }
        foreach (Favorites proj in array)
        {
            GameObject obj = Instantiate(RowPrefab);
            obj.transform.SetParent(Parent.transform);
            obj.GetComponent<ProjectItem>().SetParams(proj.GetName(), proj.GetPath(), proj.GetFavType(), proj.GetTemplate());
        }
    }

    public void SetList(Favorites[] array, bool isMasterPlan)
    {
        for (int i = 0; i < Parent.transform.childCount; i++)
        {
            Destroy(Parent.transform.GetChild(i).gameObject);
        }
        foreach (Favorites proj in array)
        {
            GameObject obj = Instantiate(RowPrefab);
            obj.transform.SetParent(Parent.transform);
            obj.GetComponent<ProjectItem>().SetParams(proj.GetName(), proj.GetPath(), proj.GetFavType(), proj.GetTemplate(), isMasterPlan);
        }
    }

    void SetSearchResult(InputField input)
    {
        string searchStr = input.text;
        List<Favorites> tmpArray = new List<Favorites>();
        foreach (GameObject proj in projects)
        {
            if(proj.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<Point>().Name.ToLower().IndexOf(searchStr.ToLower()) >= 0)
            {
                string _name = proj.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<Point>().Name;
                string _path = proj.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<Point>().Path;
                int _type = proj.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<Point>().Type;
                int _template = proj.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<Point>().Template;
                tmpArray.Add(new Favorites(_name, _path, _type, _template));
            }
        }
        SetList(tmpArray.ToArray());
    }

    public void ProjectFilterByIndexArray(int[] indexArray)
    {
        List<Favorites> tmpArray = new List<Favorites>();
        foreach (int index in indexArray)
        {
            if (index >= 0 && index < projectsList.Count - 1)
            {
                tmpArray.Add(projectsList[index]);
            }
        }
        SetList(tmpArray.ToArray(), true);
    }

    public bool GetIsPanelShow() { return isPanelShow; }

    public void ShowProjectsList()
    {
        if (!isPanelShow)
        {
            StartCoroutine("Show");
            isPanelShow = true;
        }  
        else
        {
            StartCoroutine("Hide");
            isPanelShow = false;
        }

    }

    IEnumerator Show()
    {
        ShowPanelBtn.transform.parent.gameObject.SetActive(false);
        for (float i = 0; i <= 1.1f; i += 0.1f)
        {
            MainPanel.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(startPos, Vector2.zero, i);
            yield return null;
        }
}
    IEnumerator Hide()
    {
        for (float i = 0; i <= 1.1f; i += 0.1f)
        {
            MainPanel.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(Vector2.zero, startPos, i);
            yield return null;
        }
        ShowPanelBtn.transform.parent.gameObject.SetActive(true);
    }

    void ShowFavorites()
    {
        if (ProjectsScript.instance.GetIsPanelShow())
        {
            ProjectsScript.instance.ShowProjectsList();
            ProjectsScript.instance.wasShowing = true;
        }
        GameObject MainFavoritesCanvas = GameObject.FindGameObjectWithTag("FavoritesPanel");
        MainFavoritesCanvas.transform.GetChild(0).gameObject.SetActive(true);
    }

    public void SetCanBeMoving(bool val)
    {
        MovingCamera.instance.canBeMoving = val;
    }

    public void OnPointerDown()
    {
        if(MovingCamera.instance != null)
            MovingCamera.instance.canBeMoving = false;
        Debug.Log("Clicked: DOWN");
    }
    public void OnPointerUp()
    {
        if (MovingCamera.instance != null)
            MovingCamera.instance.canBeMoving = true;
        Debug.Log("Clicked: UP");
    }

}
