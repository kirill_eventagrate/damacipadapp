﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class UIManager : MonoBehaviour {

    static public UIManager instance = null;

    public GameObject MainCanvas;
    public GameObject BigPicCanvas;
    public GameObject[] ShowWorkAreaArray;
    public Button[] Btns;


    public GameObject Logo;
    public GameObject MainImgHoriz;
    public GameObject MainImgVert;
    public Text Desc;
    public Text DescVert;
    public GameObject previewimage;
    public GameObject interiorContent;
    public GameObject exteriorContent;
    public GameObject masterPlanContent;
    public VideoPlayer videoPlayer;
    public GameObject playVideoBtn;
    public GameObject rowPrefab;

    public bool isTest;

    GameObject row;
    bool isVideoPlaying = false;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Use this for initialization
    void Start()
    {
        if (isTest)
        {
            Dictionary<string, List<string>> pointData = new Dictionary<string, List<string>>();
            pointData = FileManager.instance.GetDataForPin("PinsContent/UAE/DAMACMaisonPrivé/");
            // pointData = FileManager.instance.GetDataForPin("PinsContent/UAE/MarinaTerrace/");
            SetPointData(pointData, 0);
        }
        playVideoBtn.GetComponent<Button>().onClick.AddListener(() => PlayVideo());
    }

    public void SetPointData(Dictionary<string, List<string>> pointData, int TemplateIndex)
    {
        ShowUI(true);
        
        SetImageFromFile(MainImgHoriz, pointData["masterPic"][0]);
        SetImageFromFile(MainImgVert, pointData["masterPic"][0]);
        SetImageFromFile(Logo, pointData["logo"][0]);
        SetImageArray(interiorContent, pointData["interior"], "interior");
        SetImageArray(exteriorContent, pointData["exterior"], "exterior");
        SetImageArray(masterPlanContent, pointData["plan"], "plan");
        SetVideo(pointData["video"][0]);
        SetDescText(Desc, pointData["info"][0]);
        SetDescText(DescVert, pointData["info"][0]);
        ShowWorkAreaArray[0].transform.GetChild(0).gameObject.SetActive(false);
        ShowWorkAreaArray[0].transform.GetChild(1).gameObject.SetActive(false);
        ShowWorkAreaArray[0].transform.GetChild(TemplateIndex).gameObject.SetActive(true);
        MovingCamera.instance.canBeMoving = false;
    }

    void SetImageArray(GameObject parent, List<string> files, string tag)
    {
        for (int i = 0; i < parent.transform.childCount; i++) {
            Destroy(parent.transform.GetChild(i).gameObject);
        }
        int x = 0;
        for (int i = 0; i < files.Count; i++)
        {
            if ((x) % 4 == 0)
            {
                row = Instantiate(rowPrefab, Vector3.zero, Quaternion.identity);
                row.transform.parent = parent.transform;
                x = 0;
            }
            GameObject img = Instantiate(previewimage, Vector3.zero, Quaternion.identity);
            img.transform.SetParent(row.transform);
            img.tag = tag;
            img.GetComponent<ImgItem>().tag = tag;
            img.GetComponent<ImgItem>().index = i;
            SetImageFromFile(img, files[i]);
            x++;
        }
    }

    void SetDescText(Text txt, string file)
    {
        StreamReader reader = new StreamReader(file, System.Text.Encoding.GetEncoding(1252));
        string text = reader.ReadToEnd();
        reader.Close();
        txt.text = text;
    }

    static public void SetImageFromFile(GameObject img, string file)
    {
        Texture2D tex = null;
        byte[] fileData;
        string filePath = file;
        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            Sprite mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            img.GetComponent<Image>().sprite = mySprite;
            img.GetComponent<Image>().preserveAspect = true;
        }
    }

    public void SetVideo(string file)
    {
        videoPlayer.targetTexture.Release();
        videoPlayer.url = file;
        videoPlayer.Prepare();
    }

    public void PlayVideo()
    {
        if (!isVideoPlaying) {
            videoPlayer.Play();
            isVideoPlaying = true;
            Color fadeOutColor = new Color(255, 255, 255, 0);
            playVideoBtn.GetComponent<Image>().color = fadeOutColor;
        }
        else
        {
            videoPlayer.Pause();
            isVideoPlaying = false;
            Color fadeOutColor = new Color(255, 255, 255, 255);
            playVideoBtn.GetComponent<Image>().color = fadeOutColor;
        }
    }


    public void ShowWorkArea(int areaIndex)
    {
        for (int i = 0; i < ShowWorkAreaArray.Length; i++)
        {
            if (i == areaIndex)
                ShowWorkAreaArray[i].SetActive(true);
            else
                ShowWorkAreaArray[i].SetActive(false);
        }
        if (Btns[1])
        {
            for (int i = 1; i < Btns.Length; i++)
            {
                if (i == areaIndex)
                    Btns[i].interactable = false;
                else
                    Btns[i].interactable = true;
            }
        }
    }

    public void ShowUI(bool val)
    {
        MainCanvas.SetActive(val);
        MovingCamera.instance.canBeMoving = true;
        ShowWorkArea(0);
        if (!val) {
            if (ProjectsScript.instance.wasShowing)
            {
                ProjectsScript.instance.wasShowing = false;
                ProjectsScript.instance.ShowProjectsList();
            }
        }
    }

    public void ShowBigPic(bool val)
    {
        BigPicCanvas.SetActive(val);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
