﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FileManager : MonoBehaviour {

    [HideInInspector]
    static public FileManager instance = null;

    public Image img;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetRootFolder()
    {
        Debug.Log(Application.persistentDataPath + "/");
    }

    public Dictionary<string, List<string>> GetDataForPin(string path)
    {
        Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
        string root = Application.persistentDataPath + "/";
        path = root + path;
        Debug.Log(path);
        if (Directory.Exists(path))
        {
            result.Add("exterior", GetFileList(path + "exterior/", "*.png"));
            result.Add("info", GetFileList(path + "info/", "*.txt"));
            result.Add("interior", GetFileList(path + "interior/", "*.png"));
            result.Add("logo", GetFileList(path + "logo/", "*.png"));
            result.Add("masterPic", GetFileList(path + "masterPic/", "*.png"));
            result.Add("plan", GetFileList(path + "plan/", "*.png"));
            result.Add("video", GetFileList(path + "video/", "*.mp4"));
        }
        return result;
    }

    public List<string> GetFileList(string path, string fileType){
        List<string> resultArray = new List<string>();
        if (Directory.Exists(path))
        {
            var info = new DirectoryInfo(path);
            FileInfo[] fileList;
            fileList = info.GetFiles(fileType);
            foreach (var file in fileList)
            {
                resultArray.Add(file.ToString());
                //Debug.Log("file: " + file);
            }
        }
        return resultArray;
    }

}
