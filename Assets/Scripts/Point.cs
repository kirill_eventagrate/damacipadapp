﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour {

    Camera m_Camera;
    FileManager fm;
    Vector3 StartScale;
    public string Name;
    public string Path;
    public int Type = 0;
    public int Template = 0;
    public GameObject ProjectArra;
    public GameObject LegendOfPins;

    Vector2 startPos;
    bool isLegendShow = false;
    // Use this for initialization
    void Start () {
        if (LegendOfPins != null)
        {
            startPos = LegendOfPins.GetComponent<RectTransform>().anchoredPosition;
        }
        m_Camera = Camera.main;
        StartScale = transform.parent.transform.localScale;
        transform.parent.transform.localScale = Vector3.zero;
        //ShowPoint(3f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LateUpdate()
    {
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
    }

    public void ShowPoint(float _delay)
    {
        iTween.ScaleTo(transform.parent.transform.gameObject, iTween.Hash("scale", StartScale, "delay", _delay, "time", 1f));
    }

    public void HidePoint(float _time)
    {
        iTween.ScaleTo(transform.parent.transform.gameObject, iTween.Hash("scale", Vector3.zero, "time", 1f));
    }

    public void GetPointInfo()
    {
        if (ProjectsScript.instance.GetIsPanelShow())
        {
            ProjectsScript.instance.ShowProjectsList();
            ProjectsScript.instance.wasShowing = true;
        } 
        Dictionary<string, List<string>> pointData = new Dictionary<string, List<string>>();
        string country = PlayerPrefs.GetString("Country");
        pointData = FileManager.instance.GetDataForPin("PinsContent/" + country + "/" + Path);
        UIManager.instance.SetPointData(pointData, Template);
    }
    
    private void OnMouseUpAsButton()
    {
        if (Type == 0) {
            GetPointInfo();
        } else if (Type == 1) {
            m_Camera.GetComponentInParent<MapSceneScript>().GoToProjectPosition(transform.position, Name);
            GameStateMode.instance.isBackMovingMap2 = true;
            GameStateMode.instance.SetMap2Values(1, transform.position, MapSceneScript.instance.transform.GetChild(0).transform.eulerAngles, MapSceneScript.instance.transform.position, MapSceneScript.instance.transform.GetChild(0).transform.eulerAngles);
        } else if (Type == 2) {
            Vector3 newPos = new Vector3(transform.position.x, transform.position.y + 0.012f, transform.position.z - 0.01f);
            m_Camera.GetComponentInParent<MapSceneScript>().GoToProjectPosition(newPos);
            m_Camera.GetComponentInParent<MapSceneScript>().HideAllPoints();
            if (ProjectArra)
            {
                ShowProjects();
            }
            if (LegendOfPins != null)
            {
                StartCoroutine(Show());
            }
        }
    }
    public void ShowProjects()
    {
        if (ProjectArra)
        {
            for (int i = 0; i < ProjectArra.transform.childCount; i++)
            {
                iTween.ScaleTo(ProjectArra.transform.GetChild(i).transform.GetChild(0).gameObject, iTween.Hash("scale", Vector3.one, "time", 1f));
            }
        } 
    }
    public void HideProjects()
    {
        if (ProjectArra)
        {
            for (int i = 0; i < ProjectArra.transform.childCount; i++)
            {
                iTween.ScaleTo(ProjectArra.transform.GetChild(i).transform.GetChild(0).gameObject, iTween.Hash("scale", Vector3.zero, "time", 1f));
            }
            if (isLegendShow)
            {
                StartCoroutine(Hide());
            }
        }
    }



    IEnumerator Show()
    {
        for (float i = 0; i <= 1.1f; i += 0.1f)
        {
            LegendOfPins.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(startPos, Vector2.zero, i);
            yield return null;
        }
        isLegendShow = true;
    }
    IEnumerator Hide()
    {
        for (float i = 0; i <= 1.1f; i += 0.1f)
        {
            LegendOfPins.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(Vector2.zero, startPos, i);
            yield return null;
        }
        isLegendShow = false;
    }
}
