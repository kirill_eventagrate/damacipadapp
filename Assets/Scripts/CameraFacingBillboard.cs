﻿using UnityEngine;
public class CameraFacingBillboard : MonoBehaviour
{
    Camera m_Camera;
    public string country = "";
    public Transform countryBaseTarget;

    private void Start()
    {
        m_Camera = Camera.main;
    }

    private void LateUpdate()
    {
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
    }

    public void OnMouseUp()
    {
        PlayerPrefs.SetString("Country", country);

        GameStateMode.instance.isBackMovingMap1 = true;
        GameStateMode.instance.SetMap2Values(0, countryBaseTarget.position, countryBaseTarget.eulerAngles, m_Camera.transform.position, m_Camera.transform.eulerAngles);

        GameStateMode.instance.countryEulerAngles = m_Camera.transform.localEulerAngles;
        GameStateMode.instance.countryPos = m_Camera.transform.position;
        GameStateMode.instance.targetOnDown = m_Camera.GetComponent<CameraOrbit>().targetOnDown;

        GameObject.FindObjectOfType<CameraOrbit>().targetCountry = countryBaseTarget;
        GameObject.FindObjectOfType<CameraOrbit>().moveTowardsDestination = true;

    }
}