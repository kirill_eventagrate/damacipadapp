﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ForTestScripts : MonoBehaviour {

    public GameObject Logo;
    public GameObject previewimage;
    public GameObject content;
    public GameObject rowPrefab;

    GameObject row;
    // Use this for initialization
    void Start () {
        Dictionary<string, List<string>> pointData = new Dictionary<string, List<string>>();
        pointData = FileManager.instance.GetDataForPin("PinsContent/UAE/DAMACMaisonPrivé/");
        SetImageFromFile(Logo, pointData["logo"][0]);
        int x = 0;
        foreach (var file in pointData["plan"])
        {
            if ((x) % 4 == 0)
            {
                row = Instantiate(rowPrefab, Vector3.zero, Quaternion.identity);
                row.transform.parent = content.transform;
                x = 0;
            }
            GameObject img = Instantiate(previewimage, Vector3.zero, Quaternion.identity);
            img.transform.parent = row.transform;
            SetImageFromFile(img, file);
            x++;
        }
    }

    void SetImageFromFile(GameObject img, string file)
    {
        Texture2D tex = null;
        byte[] fileData;
        string filePath = file;
        if (File.Exists(filePath)) // ERROR: The name 'File' does not exist in the current context?
        {
            fileData = File.ReadAllBytes(filePath); // ERROR: The name 'File' does not exist in the current context?
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            Sprite mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            img.GetComponent<Image>().sprite = mySprite;
            img.GetComponent<Image>().preserveAspect = true;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
