﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FavoritesManager : MonoBehaviour {

    public GameObject container;
    public GameObject RowPrefab;
    public bool isTest = false;
    // Use this for initialization
    void Start()
    {
        if (isTest)
        {
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Project"))
            {
                string _name = obj.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Point>().Name;
                string _path = obj.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Point>().Path;
                int _type = obj.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Point>().Type;
                int _template = obj.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Point>().Template;
                FavoritesScript.instance.AddFavorites(_name, _path, _type, _template);
            }
        }
        SetFavorites();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetFavorites()
    {
        SetList(FavoritesScript.instance.favorites.ToArray());
    }

    void SetList(Favorites[] array)
    {
        for (int i = 0; i < container.transform.childCount; i++)
        {
            Destroy(container.transform.GetChild(i).gameObject);
        }
        foreach (Favorites proj in array)
        {
            GameObject obj = Instantiate(RowPrefab);
            obj.transform.SetParent(container.transform);
            obj.GetComponent<ProjectItem>().SetParams(proj.GetName(), proj.GetPath(), proj.GetFavType(), proj.GetTemplate());
            obj.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => DelFavorites(proj, obj));
        }
    }
    void DelFavorites(Favorites item, GameObject row)
    {
        FavoritesScript.instance.DelFavorite(item);
        Destroy(row);
    }

    public void ShowPanel(bool val)
    {
        GameObject MainFavoritesCanvas = GameObject.FindGameObjectWithTag("FavoritesPanel");
        MainFavoritesCanvas.transform.GetChild(0).gameObject.SetActive(val);
        if (!val)
        {
            if (ProjectsScript.instance.wasShowing)
            {
                ProjectsScript.instance.wasShowing = false;
                ProjectsScript.instance.ShowProjectsList();
            }
        }
    }
}
