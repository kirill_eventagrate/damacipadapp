﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectTriggerScript : MonoBehaviour {

    public GameObject floorForMask;
    public Texture2D mask;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "MainCamera")
        {
            floorForMask.GetComponent<MeshRenderer>().materials[0].SetTexture("_Detail", mask);
        }
    }
}
