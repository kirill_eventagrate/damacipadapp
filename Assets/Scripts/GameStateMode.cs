﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateMode : MonoBehaviour {

    static public GameStateMode instance = null;


    GameObject TargetObjMov;
    GameObject TargetObjRot;
    
    float movingTime = 1f;
    float rotationTime = 1f;
    float delayPos = 0f;
    float delayRot = 0f;

    List<Dictionary<string, Vector3[]>> MapsData = new List<Dictionary<string, Vector3[]>>();
    
    public bool isBackMovingMap1 = false;
    public bool isBackMovingMap2 = false;

    public Vector3 countryEulerAngles;
    public Vector3 countryPos;
    public Vector2 targetOnDown;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        Vector3[] tmpVec = new Vector3[2];
        tmpVec[0] = Vector3.zero;
        tmpVec[1] = Vector3.zero;
        Dictionary<string, Vector3[]> tmpDic = new Dictionary<string, Vector3[]>();
        tmpDic.Add("position", tmpVec);
        tmpDic.Add("rotation", tmpVec);
        MapsData.Add(tmpDic);
        MapsData.Add(tmpDic);
    }

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetMap2Values(int MapIndex, Vector3 startPos, Vector3 startRot, Vector3 endPos, Vector3 endRot)
    {
        Vector3[] tmpVec = new Vector3[2];
        tmpVec[0] = startPos;
        tmpVec[1] = endPos;

        Vector3[] tmpVecRot = new Vector3[2];
        tmpVecRot[0] = startRot;
        tmpVecRot[1] = endRot;

        Dictionary<string, Vector3[]> tmpDic = new Dictionary<string, Vector3[]>();
        tmpDic.Add("position", tmpVec);
        tmpDic.Add("rotation", tmpVecRot);

        MapsData[MapIndex] = tmpDic;
    }

    public void SetParams(float _movingTime, float _rotationTime, float _delayPos, float _delayRot)
    {
        movingTime = _movingTime;
        rotationTime = _rotationTime;
        delayPos = _delayPos;
        delayRot = _delayRot;
    }
    public void SetTargetObj(GameObject _TargetObjMov, GameObject _TargetObjRot)
    {
        TargetObjMov = _TargetObjMov;
        TargetObjRot = _TargetObjRot;
    }
    public void Moving(int MapIndex)
    {
        TargetObjMov.transform.position = MapsData[MapIndex]["position"][0];
        TargetObjRot.transform.eulerAngles = MapsData[MapIndex]["rotation"][0];
        iTween.MoveTo(TargetObjMov, iTween.Hash("position", MapsData[1]["position"][1], "time", movingTime, "delay", delayPos, "easetype", iTween.EaseType.easeOutQuint));
        iTween.RotateTo(TargetObjRot, iTween.Hash("rotation", MapsData[1]["rotation"][1], "time", rotationTime, "delay", delayRot, "easetype", iTween.EaseType.easeInOutQuad));
    }

}