﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImgItem : MonoBehaviour {

    public string tag = "";
    public int index = 0;
    Button btn;
	// Use this for initialization
	void Start () {
        btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(() => ShowImage());
	}

    public void ShowImage() {
        UIManager.instance.ShowBigPic(true);
        ShowBigPicScript.instance.SetImages(tag, index);

    }

    public void FadeIn()
    {
        StartCoroutine("Hide");
    }

    IEnumerator Hide()
    {
        for(float i = 0; i <= 1; i += 0.1f)
        {
            yield return new WaitForSeconds(.1f);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
