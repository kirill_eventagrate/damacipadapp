﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSceneScript : MonoBehaviour {

    static public MapSceneScript instance = null;


    string MapName;
    public float orthoZoomSpeed = 0.5f;
    public GameObject UAE;
    public GameObject UK;
    public GameObject Saudia;
    public GameObject Jordan;
    public GameObject Lebanon;


    Transform startMarker;
    Vector3 startPosition;
    Vector3 lastPosition;
    Quaternion startRotation;
    public Transform endMarker;
    public float speed = 2.0F;
    float pointOfRotation = 1f;
    bool goForward = true;

    float[] BoundsX = new float[] { -1f, 2f };
    float[] BoundsY = new float[] { 0.025f, 3f };
    float[] BoundsZ = new float[] { -3f, 2f };


    Dictionary<string, GameObject> Maps = new Dictionary<string, GameObject>();
    GameObject[] ProjectArea;
    List<Vector3> positions = new List<Vector3>();


    void Awake()
    {
        if(instance == null) { instance = this; }
        if (GameStateMode.instance == null)
        {
            GameObject obj = new GameObject();
            obj.AddComponent<GameStateMode>();
        }
       
        MapName = PlayerPrefs.GetString("Country");
        Debug.Log(MapName);
        Maps.Add("UAE", UAE);
        Maps.Add("UK", UK);
        Maps.Add("Saudia", Saudia);
        Maps.Add("Jordan", Jordan);
        Maps.Add("Lebanon", Lebanon);
        if (Maps[MapName])
        {
            Instantiate(Maps[MapName], Vector3.zero, new Quaternion(0, -180, 0, 0));  // Quaternion.identity
        }  
    }

    // Use this for initialization
    void Start () {
        GetComponent<MovingCamera>().BoundsX = BoundsX;
        GetComponent<MovingCamera>().BoundsY = BoundsY;
        GetComponent<MovingCamera>().BoundsZ = BoundsZ;
        endMarker = GameObject.FindGameObjectWithTag("EndTarget").transform;
        startMarker = gameObject.transform;
        startPosition = gameObject.transform.position;
        startRotation = transform.localRotation;
        ProjectArea = GameObject.FindGameObjectsWithTag("ProjectArea");
        GameStateMode.instance.SetTargetObj(gameObject, transform.GetChild(0).gameObject);
        if (!GameStateMode.instance.isBackMovingMap2)
        {
            GameStateMode.instance.SetMap2Values(1, startPosition, transform.GetChild(0).gameObject.transform.eulerAngles, endMarker.position, endMarker.eulerAngles);
            GameStateMode.instance.SetParams(3.0f, 1.0f, 0f, 1.5f);
            Loom.QueueOnMainThread(() => {
                ShowAllPoints(2.8f);
            }, 0.2f);
            
        }
        else
        {
            GameStateMode.instance.SetParams(1.0f, 1.0f, 0f, 0f);
            Loom.QueueOnMainThread(() => {
                ShowAllPoints(.75f);
            }, 0.2f);
        }
        positions.Add(endMarker.position);

        DoubleClickScript.instanse.SetAction(() => Moving());

        GameStateMode.instance.Moving(1);
    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }

    void HideProjectsPins()
    {
        foreach (GameObject proj in ProjectArea)
        {
            proj.transform.GetChild(0).transform.GetChild(1).gameObject.GetComponent<Point>().HideProjects();
        }
    }

    public void ShowAllPoints(float _delay)
    {
        foreach (GameObject proj in ProjectArea)
        {
            proj.transform.GetChild(0).transform.GetChild(1).gameObject.GetComponent<Point>().ShowPoint(_delay);
        }
        HideProjectsPins();
    }

    public void HideAllPoints()
    {
        foreach (GameObject proj in ProjectArea)
        {
            proj.transform.GetChild(0).transform.GetChild(1).gameObject.GetComponent<Point>().HidePoint(1f);
        }
    }

    public void GoBack()
    {
        if (positions.Count > 1) {
            if (transform.position != positions[positions.Count - 1])
            {
                iTween.MoveTo(gameObject, iTween.Hash("position", positions[positions.Count - 1], "time", 0.8f, "easetype", iTween.EaseType.easeOutCubic));
                positions.RemoveAt(positions.Count - 1);
                ShowAllPoints(0);
            }
        } else if (transform.position != positions[0]) {
            iTween.MoveTo(gameObject, iTween.Hash("position", positions[0], "time", 0.8f, "easetype", iTween.EaseType.easeOutCubic));
        } else {
            iTween.MoveTo(gameObject, iTween.Hash("position", startPosition, "time", 3.0f, "easetype", iTween.EaseType.easeOutCubic));
            iTween.RotateTo(transform.GetChild(0).gameObject, iTween.Hash("rotation", new Vector3(90, 0, 0), "time", 1.5f, "easetype", iTween.EaseType.easeOutCubic));
            Invoke("StartToFadeOut", 1.5f);
            GameStateMode.instance.isBackMovingMap2 = false;
        }
    }
    void StartToFadeOut()
    {
        gameObject.GetComponent<FadeOut>().nextScene = "Main";
        gameObject.GetComponent<FadeOut>().StartFade();
    }

    public void GoToProjectPosition(Vector3 newPosition, string projectName)
    {
        PlayerPrefs.SetString("ProjectMaps", projectName);
        iTween.MoveTo(gameObject, iTween.Hash("position", newPosition, "time", .8f, "easetype", iTween.EaseType.easeOutCubic));
        StartToFadeToLevel3();
    }
    public void GoToProjectPosition(Vector3 newPosition)
    {
        positions.Add(transform.position);
        iTween.MoveTo(gameObject, iTween.Hash("position", newPosition, "time", .8f, "easetype", iTween.EaseType.easeOutCubic));
    }
    void StartToFadeToLevel3()
    {
        gameObject.GetComponent<FadeOut>().nextScene = "Level3";
        gameObject.GetComponent<FadeOut>().StartFade();
    }

    void Moving()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.collider.name);
        }
    }

}
