﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FavoritesScript : MonoBehaviour {

    static public FavoritesScript instance = null;

    public List<Favorites> favorites = new List<Favorites>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void AddFavorites(string _name, string _path, int _type, int _template)
    {
        favorites.Add(new Favorites(_name, _path, _type, _template));
    }
    public void DelFavorite(Favorites item)
    {
        favorites.Remove(item);
    }
}

public class Favorites
{
    string Name = "";
    string Path = "";
    int Type = 0;
    int Template = 0;
    public Favorites(string _name, string _path, int _type, int _template)
    {
        Name = _name;
        Path = _path;
        Type = _type;
        Template = _template;
    }
    public string GetName() { return Name; }
    public string GetPath() { return Path; }
    public int GetFavType() { return Type; }
    public int GetTemplate() { return Template; }
}