﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Level3Script : MonoBehaviour {

    public Camera cam;
    public GameObject Akoya;
    public GameObject DamacHills;

    Dictionary<string, MasterPlanData> ProjectMaps = new Dictionary<string, MasterPlanData>();

    public struct MasterPlanData
    {
        public GameObject prefab;
        public List<Favorites> favoritesList;

        public MasterPlanData(GameObject _prefab, List<Favorites> _favoritesList)
        {
            prefab = _prefab;
            favoritesList = _favoritesList;
        }
    }

    float dawnTime;
    float upTime;
    string MapName;
    GameObject Map;
    Vector3 startPos;
    Vector3 startPosOfMap;
    float[] BoundsX = new float[] { -18f, 18f };
    float[] BoundsY = new float[] { 5f, 16f };
    float[] BoundsZ = new float[] { -18f, 4f };

    List<Favorites> masterPlanProjects = new List<Favorites>();

    // Use this for initialization
    void Start () {
        GetComponent<MovingCamera>().BoundsX = BoundsX;
        GetComponent<MovingCamera>().BoundsY = BoundsY;
        GetComponent<MovingCamera>().BoundsZ = BoundsZ;
        startPos = transform.position;
        MapName = PlayerPrefs.GetString("ProjectMaps");
        //Debug.Log(MapName);

        masterPlanProjects.Add(new Favorites("Just Cavalli Villas", "__Masterplans/AkoyaOxygen/Content/JustCavalliVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("Aknan Villas", "__Masterplans/AkoyaOxygen/Content/AknanVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("Aurum Villas", "__Masterplans/AkoyaOxygen/Content/AurumVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("Bait Al Aseel", "__Masterplans/AkoyaOxygen/Content/BaitAlAseel/", 0, 1));
        masterPlanProjects.Add(new Favorites("Biela Villas", "__Masterplans/AkoyaOxygen/Content/BielaVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("Hajar Villas", "__Masterplans/AkoyaOxygen/Content/HajarVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("Mega Villas", "__Masterplans/AkoyaOxygen/Content/MegaVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("Sahara Villas", "__Masterplans/AkoyaOxygen/Content/SaharaVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("Casablanca Villas", "__Masterplans/AkoyaOxygen/Content/CasablancaVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("The Ultimate Luxury Collection", "__Masterplans/AkoyaOxygen/Content/TheUltimateLuxuryCollection/", 0, 1));
        masterPlanProjects.Add(new Favorites("Fiora", "__Masterplans/AkoyaOxygen/Content/Fiora/", 0, 0));

        ProjectMaps.Add("Akoya", new MasterPlanData(Akoya, masterPlanProjects));
        masterPlanProjects.Clear();

        masterPlanProjects.Add(new Favorites("The Trump Estates", "__Masterplans/DamacHills/Content/TheTrumpEstates/", 0, 1));
        masterPlanProjects.Add(new Favorites("Radisson Hotel, Dubai DAMAC Hills", "__Masterplans/DamacHills/Content/RadissonHotel,DubaiDAMACHills/", 0, 1));
        masterPlanProjects.Add(new Favorites("DAMAC Villas by Paramount Hotels & Resorts Dubai", "__Masterplans/DamacHills/Content/DAMACVillasbyParamountHotels&ResortsDubai/", 0, 1));
        masterPlanProjects.Add(new Favorites("Fendi Styled Villas", "__Masterplans/DamacHills/Content/FendiStyledVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("The Golf Villas", "__Masterplans/DamacHills/Content/TheGolfVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("The Park Villas", "__Masterplans/DamacHills/Content/TheParkVillas/", 0, 1));
        masterPlanProjects.Add(new Favorites("90210 Villas", "__Masterplans/DamacHills/Content/90210Villas/", 0, 1));
        masterPlanProjects.Add(new Favorites("Artesia", "__Masterplans/DamacHills/Content/Artesia/", 0, 1));
        masterPlanProjects.Add(new Favorites("Golf Terrace / Golf Veduta", "__Masterplans/DamacHills/Content/GolfTerraceGolfVeduta/", 0, 1));
        masterPlanProjects.Add(new Favorites("Carson", "__Masterplans/DamacHills/Content/Carson/", 0, 1));
        masterPlanProjects.Add(new Favorites("Golf Vita / SkyView Levels", "__Masterplans/DamacHills/Content/GolfVitaSkyviewLevels/", 0, 0));
        masterPlanProjects.Add(new Favorites("Apartments on The Golf", "__Masterplans/DamacHills/Content/ApartmentsonTheGolf/", 0, 0));
        masterPlanProjects.Add(new Favorites("Apartments on The Park", "__Masterplans/DamacHills/Content/ApartmentsonThePark/", 0, 0));
        masterPlanProjects.Add(new Favorites("Townhouses on The Golf & The Park", "__Masterplans/DamacHills/Content/TownhousesonTheGolf&ThePark/", 0, 0));

        ProjectMaps.Add("DamacHills", new MasterPlanData(DamacHills, masterPlanProjects));

        if (ProjectMaps[MapName].prefab)
        {
            startPosOfMap = new Vector3(17f, -7f, -12f);
            Map = Instantiate(ProjectMaps[MapName].prefab, startPosOfMap, new Quaternion(0, 180, 0, 0));
            iTween.MoveTo(Map, iTween.Hash("position", new Vector3(4.5f, 0, 0), "time", 1.0f, "easetype", iTween.EaseType.easeOutCubic));
            ProjectsScript.instance.SetProjects(ProjectMaps[MapName].favoritesList);
        }


        if(ProjectsScript.instance != null)
        {
            List<Favorites> projectsList = new List<Favorites>();
            GameObject[] projects = GameObject.FindGameObjectsWithTag("Clusters");
            Loom.QueueOnMainThread(() => {
                ProjectsScript.instance.ShowProjectsList();
            }, .5f);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.touchCount == 1)
        {
            
        }

        // Check if there is a touch
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            // Check if finger is over a UI element
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                MovingCamera.instance.canBeMoving = false;
            }
            else
            {
                MovingCamera.instance.canBeMoving = true;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            dawnTime = Time.time;
            // Check if the mouse was clicked over a UI element
            /*if (EventSystem.current.IsPointerOverGameObject())
            {
                MovingCamera.instance.canBeMoving = false;
            }
            else
            {
                MovingCamera.instance.canBeMoving = true;
            }*/
        }
        if (Input.GetMouseButtonUp(0))
        {
            upTime = Time.time;
            if (MovingCamera.instance.canBeMoving)
            {
                if ((upTime - dawnTime) <= 0.2)
                {
                    RaycastHit hit;
                    Ray ray = cam.ScreenPointToRay(Input.mousePosition);

                    if (Physics.Raycast(ray, out hit))
                    {
                        Transform objectHit = hit.transform;
                        if (objectHit.tag == "Clusters")
                        {
                            objectHit.GetComponent<ClusterItem>().SetMask();
                            Vector3 newPosition = new Vector3(objectHit.position.x, objectHit.position.y + 5, objectHit.position.z - 3);
                            iTween.MoveTo(gameObject, iTween.Hash("position", newPosition, "time", 1.0f, "easetype", iTween.EaseType.easeOutCubic));
                        }
                        if (objectHit.tag == "ClusterTag")
                        {
                            objectHit.parent.gameObject.GetComponent<ClusterItem>().SetMask();
                            Vector3 newPosition = new Vector3(objectHit.parent.transform.position.x, objectHit.parent.transform.position.y + 5, objectHit.parent.transform.position.z - 3);
                            iTween.MoveTo(gameObject, iTween.Hash("position", newPosition, "time", 1.0f, "easetype", iTween.EaseType.easeOutCubic));
                        }
                    }
                }
            }
        }
    }

    IEnumerator Show()
    {
        for (float i = 0; i <= 1.1f; i += 0.04f)
        {
            Map.transform.position = Vector3.Lerp(startPos, Vector3.zero, i);
            yield return new WaitForEndOfFrame(); 
        }
    }
    public GameObject GetMainMap()
    {
        return Map;
    }

    public void GoBack()
    {
        ProjectsScript.instance.SetProjects(ProjectMaps[MapName].favoritesList);
        iTween.MoveTo(gameObject, iTween.Hash("position", startPos, "time", .5f, "easetype", iTween.EaseType.easeOutCubic));
        if (Vector3.Distance(startPos, transform.position) == 0)
        {
            iTween.MoveTo(Map, iTween.Hash("position", startPosOfMap, "time", 1.0f, "easetype", iTween.EaseType.easeOutCubic));
            StartToFadeOut();
        }else if (!ProjectsScript.instance.GetIsPanelShow())
        {
            ProjectsScript.instance.ShowProjectsList();
        }
    }
    void StartToFadeOut()
    {
        gameObject.GetComponent<FadeOut>().nextScene = "MapScene";
        gameObject.GetComponent<FadeOut>().StartFade();
    }
}

class MasterPlanProject {
    string Name;
    string Path;
    int Template;

    public MasterPlanProject(string _name, string _path, int _template)
    {
        Name = _name;
        Path = _path;
        Template = _template;
    }

    public string GetPath() { return Path; }
    public int GetTemplate() { return Template; }
}
