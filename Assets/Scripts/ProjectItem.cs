﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProjectItem : MonoBehaviour
{

    public string Name;
    public string Path;
    public int Type;
    public int Template;

    string rootPath;
    Button btn;
    // Use this for initialization
    void Start () {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() => ShowProject());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetParams(string _name, string _path, int _type, int _template)
    {
        Name = _name;
        Path = _path;
        Type = _type;
        Template = _template;
        List<string> file = new List<string>();
        rootPath = Application.persistentDataPath + "/PinsContent/" + PlayerPrefs.GetString("Country") + "/";
        file = FileManager.instance.GetFileList(rootPath + Path + "masterPic/", "*.png");
        if(file.Count > 0)
        {
            UIManager.SetImageFromFile(transform.GetChild(0).gameObject, file[0]);
        }
        transform.GetChild(1).GetComponent<Text>().text = Name;
    }

    public void SetParams(string _name, string _path, int _type, int _template, bool isMasterPlan)
    {
        Name = _name;
        Path = _path;
        Type = _type;
        Template = _template;
        List<string> file = new List<string>();
        if (isMasterPlan) {
            rootPath = Application.persistentDataPath + "/PinsContent/";
            file = FileManager.instance.GetFileList(rootPath + Path + "masterPic/", "*.png");
        } else {
            rootPath = Application.persistentDataPath + "/PinsContent/" + PlayerPrefs.GetString("Country") + "/";
            file = FileManager.instance.GetFileList(rootPath + Path + "masterPic/", "*.png");
        }
        if (file.Count > 0)
        {
            UIManager.SetImageFromFile(transform.GetChild(0).gameObject, file[0]);
        }
        transform.GetChild(1).GetComponent<Text>().text = Name;
    }

    public void ShowProject()
    {
        if (ProjectsScript.instance.GetIsPanelShow())
        {
            ProjectsScript.instance.ShowProjectsList();
            ProjectsScript.instance.wasShowing = true;
        }
        Dictionary<string, List<string>> pointData = new Dictionary<string, List<string>>();
        pointData = FileManager.instance.GetDataForPin("PinsContent/" + Path);
        UIManager.instance.SetPointData(pointData, Template);
    }

    
    
}
