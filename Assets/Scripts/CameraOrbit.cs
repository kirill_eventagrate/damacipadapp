﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour
{
    public float MinDistance = 1.0f;
    public float MaxDistance = 1.3f;
    float distance = 1000;
    float distanceTarget;
    Vector2 mouse;
    Vector2 mouseOnDown;
    Vector2 rotation;
    Vector2 target = new Vector2(Mathf.PI * 3 / 2, Mathf.PI / 6);
    public Vector2 targetOnDown;

    public bool moveTowardsDestination = false;
    bool moveTowardsOrigin = false;

    public Transform targetCountry;

    // Use this for initialization
    void Start()
    {

        distanceTarget = transform.position.magnitude;
        //Debug.Log(distanceTarget);

        if (GameStateMode.instance.isBackMovingMap1)
        {
            string country = PlayerPrefs.GetString("Country", "UAE");

            transform.position = GameObject.Find("GlobeButtonItem_" + country).transform.GetChild(0).position;
            moveTowardsOrigin = true;
            transform.localEulerAngles = GameStateMode.instance.countryEulerAngles;
            Debug.Log("Local : " + GameStateMode.instance.countryPos);
        }
        /*else
        {
            string country = "UAE";
            transform.position = GameObject.Find("GlobeButtonItem_" + country).transform.GetChild(0).position;
            transform.localEulerAngles = new Vector3(28.6f, 94.1f, 0.0f);


            moveTowardsOrigin = true;
            GameStateMode.instance.countryPos = new Vector3(-1.1f , 0.5f ,0.0f);
            GameStateMode.instance.targetOnDown = new Vector2(-1.5f, 0.5f);
        }*/

    }
    bool down = false;
    bool fadeOnce = true;

    float elapsedTime = 0;

    // Update is called once per frame
    void Update()
    {
        if (moveTowardsDestination)
        {
            float step =  Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetCountry.position, step);

            float dist = Vector3.Distance(transform.position, targetCountry.position);
            if (dist < 0.6f && fadeOnce)
            {
                fadeOnce = false;
                GameObject.FindObjectOfType<FadeOut>().StartFade();
            }
        }
        else if (moveTowardsOrigin)
        {
            elapsedTime += Time.deltaTime;

            float step = Time.deltaTime;

            transform.position = Vector3.MoveTowards(transform.position, GameStateMode.instance.countryPos, step);

            float dist = Vector3.Distance(transform.position, GameStateMode.instance.countryPos);
            if (dist < Mathf.Epsilon)
            {
                distance = 1.2f;
                distanceTarget = 1.2f;
                target = GameStateMode.instance.targetOnDown;
                rotation = target;
                targetOnDown = GameStateMode.instance.targetOnDown;
                Debug.Log("Target : " + target);
                moveTowardsOrigin = false;
            }

        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                down = true;
                mouseOnDown.x = Input.mousePosition.x;
                mouseOnDown.y = -Input.mousePosition.y;

                targetOnDown.x = target.x;
                targetOnDown.y = target.y;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                down = false;
            }
            if (down)
            {
                mouse.x = Input.mousePosition.x;
                mouse.y = -Input.mousePosition.y;

                float zoomDamp = distance / 1;

                target.x = targetOnDown.x + (mouse.x - mouseOnDown.x) * 0.001f * zoomDamp;
                target.y = targetOnDown.y + (mouse.y - mouseOnDown.y) * 0.001f * zoomDamp;

                target.y = Mathf.Clamp(target.y, -Mathf.PI / 2 + 0.01f, Mathf.PI / 2 - 0.01f);
            }

            distanceTarget -= Input.GetAxis("Mouse ScrollWheel");
            //Debug.Log("distanceTarget" + distanceTarget);
            distanceTarget = Mathf.Clamp(distanceTarget, MinDistance, MaxDistance);

            rotation.x += (target.x - rotation.x) * 0.1f;
            rotation.y += (target.y - rotation.y) * 0.1f;
            distance += (distanceTarget - distance) * 0.3f;
            Vector3 position;
            position.x = distance * Mathf.Sin(rotation.x) * Mathf.Cos(rotation.y);
            position.y = distance * Mathf.Sin(rotation.y);
            position.z = distance * Mathf.Cos(rotation.x) * Mathf.Cos(rotation.y);
            //Debug.Log(position);
            transform.position = position;
            transform.LookAt(Vector3.zero);
        }

    }
}
