﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ShowBigPicScript : MonoBehaviour {

    static public ShowBigPicScript instance = null;

    public GameObject leftBtn;
    public GameObject rightBtn;
    public GameObject closeBtn;
    public Image BigImg;


    GameObject[] imgs;
    int currectIndex = 0;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public void SetImages(string tagName, int correctImgIndex)
    {
        BigImg.gameObject.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
        BigImg.gameObject.transform.localScale = Vector3.one;
        imgs = GameObject.FindGameObjectsWithTag(tagName);
        BigImg.sprite = imgs[correctImgIndex].GetComponent<Image>().sprite;
        BigImg.preserveAspect = true;
        currectIndex = correctImgIndex;
    }

    // Use this for initialization
    void Start () {
        leftBtn.GetComponent<Button>().onClick.AddListener(() => ChangeIMage("prev"));
        rightBtn.GetComponent<Button>().onClick.AddListener(() => ChangeIMage("next"));
        closeBtn.GetComponent<Button>().onClick.AddListener(() => UIManager.instance.ShowBigPic(false));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ChangeIMage(string type)
    {
        if(type == "next" && (currectIndex + 1) < imgs.Length)
        {
            currectIndex++;
        }
        else if (type == "prev" && (currectIndex - 1) >= 0)
        {
            currectIndex--;
        }
        if (imgs[currectIndex])
        {
            BigImg.sprite = imgs[currectIndex].GetComponent<Image>().sprite;
            BigImg.gameObject.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
            BigImg.gameObject.transform.localScale = Vector3.one;
        }
    }
}
