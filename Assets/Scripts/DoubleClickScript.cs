﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleClickScript : MonoBehaviour {

    static public DoubleClickScript instanse = null;

    bool one_click = false;
    bool timer_running;
    float timer_for_double_click;

    //this is how long in seconds to allow for a double click
    float delay = 0.2f;

    Action mainAction;

    private void Awake()
    {
        if (instanse == null)
            instanse = this;
    }
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonDown(0))
        {
            if (!one_click) // first click no previous clicks
            {
                one_click = true;

                timer_for_double_click = Time.time; // save the current time
                                                    // do one click things;
            }
            else
            {
                one_click = false; // found a double click, now reset

                DoDoubleClick();
            }
        }
        if (one_click)
        {
            // if the time now is delay seconds more than when the first click started. 
            if ((Time.time - timer_for_double_click) > delay)
            {
                one_click = false;
            }
        }
    }

    public void SetAction(Action action)
    {
        mainAction = action;
    }

    public void DoDoubleClick()
    {
        try
        {
            ((Action)mainAction)();
        }
        catch
        {
        }
    }
}
